import Vue from "vue";
// import VueResource from "vue-resource";
import axios from "axios";
// import xml2json from "@/lib/jquery.xml2json.js";
import { Loading, MessageBox, Message, Notification } from "element-ui";
import Lodash from "lodash";

Vue.use(Loading.directive);
Vue.use(require("vue-moment"));

export default {
	getDataCRM(obj, path) {
		const val = Lodash.get(obj);
		if (Lodash.isNil(val)) {
			return [];
		}

		if (!Lodash.isArray(val)) {
			return [val];
		}

		return val;
	},
    showConfirm(title, message, event) {
        return new Promise((resolve, reject) => {
            MessageBox.confirm(message, "Cảnh báo", {
                confirmButtonText: "Xóa",
                cancelButtonText: "Bỏ qua",
                type: "warning"
            })
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    // reject();
                });
        });
    },

    /*param: toUpperOrLowerCaseKey
    true if key to be Uppercase
    false if key to be lowercase
    undefined if do nothing*/
    rotateDementionArr(arr, fieldTobeKey, toUpperOrLowerCaseKey) {
        let retObj = {};
        arr.filter(item => {
            let key = item[fieldTobeKey];
            if (toUpperOrLowerCaseKey != undefined) {
                key = toUpperOrLowerCaseKey ? key.toUpperCase() : key.toLowerCase();
            }
            retObj[key] = item;
        });
        return retObj;
    },

    showLoading(type, load) {
    	let reLoading = null;
        switch (type) {
            case "start":
                const loading = Loading.service({
                    lock: true,
                    text: "Loading",
                    spinner: "el-icon-loading",
                    background: "rgba(0, 0, 0, 0.7)"
                });
				reLoading = loading;
				break;
            case "stop":
                if (load) load.close();
                else Loading.service().close();
                break;
            default:
                break;
        }
    },
    getDataWithRoot(data, path) {
        if (!path) {
            path = "";
        }
        let arrPath = path.split(".");
        for (let i = 0; i < arrPath.length; i++) {
            if (arrPath[i]) {
                data = Lodash.cloneDeep(data[arrPath[i]]);
                if (data == undefined) {
                    data = [];
                    break;
                }
            }
        }
        if (!data || JSON.stringify(data) === "{}") {
            return [];
        } else if (Object.prototype.toString.call(data) !== "[object Array]") {
            return [data];
        }
        return data;
    },
    readSettingNode(input, output) {
        /**
         * Create output object if not existed
         */
        if (!this.isNotEmpty(output)) {
            output = {};
        }

        $.each(input, (key, value) => {
            /**
             * Xóa các phần tử không dùng đến, có khả năng gây lỗi
             */
            // deleteAutoValue(value);
            /**
             * Kiểm tra nếu không có giá trị Description thì lấy toàn bộ giá trị của setting
             * Nếu có giá trị Description thì chỉ lấy giá trị của Description thôi
             */
            output[value.Name] = !this.isNotEmpty(value.Description)
                ? value
                : value.Description;
            if (this.isNotEmpty(value.children)) {
                output = this.readSettingNode(value.children, output);
                delete value.children;
            }
        });
        return output;
    },

    isNotEmpty(params, path, returnData) {
        let object = params;
        if (path && path !== "") {
            let arrPath = path.split(".");
            for (let i = 0; i < arrPath.length; i++) {
                if (object === null || object === undefined) {
                    break;
                } else {
                    object = object[arrPath[i].trim()];
                }
            }
        }
        if (object === null || object === undefined) {
            return false;
        }
        if (returnData) {
            return object;
        }
        return true;
    },

    decodeHtmlEntities(str) {
        if (!str) {
            return "";
        }
        return String(str)
            .replace(/&amp;/g, "&")
            .replace(/&lt;/g, "<")
            .replace(/&gt;/g, ">")
            .replace(/&quot;/g, "\"");
    },
    stringToObject(string, separator, split) {
        if (typeof string != "string") {
            return {};
        }
        separator = separator ? separator : "&";
        split = split ? split : "=";
        string = this.decodeHtmlEntities(string.trim());
        let returnObject = {},
            arr = string.split(separator);
        $.each(arr, (ks, str) => {
            if (str.indexOf(split) > 0) {
                let keyVal = str.split(split);
                $.each(keyVal, (k, v) => {
                    keyVal[k] = v.trim();
                });
                returnObject[keyVal[0]] = keyVal[1];
            }
        });
        return returnObject;
    },
    formatDateTime(dateTime, format) {
        if (!dateTime) {
            dateTime = new Date();
        }
        if (!format) {
            format = "YYYY-MM-DDTHH:mm:ss";
        }
        return Vue.moment(dateTime).format(format);
    },
    createdListFromDataTree(tree, result) {
        tree.forEach(value => {
            let tmpData = Lodash.cloneDeep(value);
            delete tmpData.children;

            result.push(tmpData);
            if (value.children) {
                this.createdListFromDataTree(value.children, result);
            }
        });

        return result;
    },

};
