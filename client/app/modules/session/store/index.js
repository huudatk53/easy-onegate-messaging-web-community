import { ADD_MESSAGE, ADD_NOTIFICATION, SET_USER, SEARCH, CLEAR_NOTIFICATION, CLEAR_MESSAGE, SET_SESSION_CRM, CLEAR_SESSION_CRM} from "./types";

const state = {
	user: null,
	notifications: [
		// { id: 1, text: "Something happened!", time: 1, user: null }
	],
	messages: [],
	searchText: "",
	crmsession: null,
};

const mutations = {
	[ADD_MESSAGE] (state, item) {
		//state.messages.splice(0);
		state.messages.push(item);
	},

	[ADD_NOTIFICATION] (state, item) {
		//state.notifications.splice(0);
		state.notifications.push(item);
	},

	[SET_USER] (state, user) {
		state.user = user;
	},

	[SEARCH] (state, text) {
		state.searchText = text;
	},	
	[CLEAR_NOTIFICATION](state, isMarkAsRead){
		if(isMarkAsRead)
			state.notifications.splice(0);
	},
	[CLEAR_MESSAGE](state, isMarkAsRead){
		if(isMarkAsRead)
			state.messages.splice(0);
	},
	[SET_SESSION_CRM](state, session){
		state.crmsession = session;		
	},
	[CLEAR_SESSION_CRM](state){
		state.crmsession = null;		
	}

};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};