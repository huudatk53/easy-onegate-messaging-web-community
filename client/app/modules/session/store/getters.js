export function me(state) {
	return state.user;
}

export function crmsession(state) {
	return state.crmsession;
}
export function notifications(state) {
	return state.notifications;
}

export function messages(state) {
	return state.messages;
}

export function searchText(state) {
	return state.searchText;
}