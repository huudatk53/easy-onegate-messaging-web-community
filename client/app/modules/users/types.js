module.exports = {
	/**
	 * User role constants
	 */
	ROLE: [{id:"admin", name: "Administrator"},
	{id:"user",name:"User"}, {id:"guest", name:"Guest"}]
	
	
	/**
	 * User permission constants
	 */
	,
	PERM: [{id: 
		"admin", name:"Admnistration"},
		{id:"owner", name:"Owner"},
		{id:"loggedIn",name: "Logged In"},
		{id:"public", name:"Public"}
	]
	
};