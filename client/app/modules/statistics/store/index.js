const state = {
	lastUpdated: null,
	pageIds: null,
	overview: null,
	recentlyMessageAndContactCount: null,
	messageAndCommentTodayCount: null,
	topUserInteraction: null,
	customerInteractionRecently: null,
	newStatistics: null,
	pageStatistics: null,
	staffs: [],
	staffStatistics: null,
	interactionStatistics: null,
	totalInteractionStatistics: null,
	tags: null,
	tagStatistics: null,
	todayTagStatistics: null
};

import * as getters from "./getters";
import * as actions from "./actions";
import * as mutations from "./mutations";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
