module.exports = {
	settingTypes: [
		{
			id: "account",
			name: "Account",
			detail: "Detail",
			template: []
		},
		{
			id: "notifications",
			name: "Notifications",
			detail: "Detail",
			
		}		 
	],
	inputTypes: [
		{ id: "number", name: "Number", detail: "Detail" },
		{ id: "checkbox", name: "Checkbox", detail: "Detail" },
		{ id: "radio", name: "Radio", detail: "Detail" },
		{ id: "select", name: "select", detail: "Detail" },
		{ id: "date", name: "Date", detail: "Detail" },
		{ id: "dateTime", name: "DateTime", detail: "Detail" }
	],
	templates: [
		{
			id: "conversations",
			name: "Conversations",
			pid: "notifications",
			detail: "Setting notification check conversations"
		},
		{
			id: "messaging",
			name: "Messages",
			pid: "notifications",
			detail: "Setting notification check messages"
		},
		{
			id: "system",
			name: "System",
			pid: "notifications",
			detail: "Setting notification check system"
		},
		{
			id: "activity",
			pid: "notifications",
			name: "Activity",
			detail: "Setting notification check activity"
		},
		{
			id: "online",			 
			name: "OnlineUserStatus",
			pid: "system",
			detail: "Setting system check online user"
		}
	]
};
