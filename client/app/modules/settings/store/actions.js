import toastr from "../../../core/toastr";
import { LOAD, ADD, SELECT, CLEAR_SELECT, UPDATE, REMOVE, CHANGE_SORT , CHANGE_VIEWMODE,  FETCHING, NO_MORE_ITEMS, CLEAR} from "./types"; 
export const NAMESPACE = "/api/settings";
 
export const getRows = function ({commit, state}, loadMore) {
	commit(FETCHING, true);
	return window.settingService.rest("find", { filter: state.viewMode}).then((data) => {
		if (data.length == 0)
			commit(NO_MORE_ITEMS);
		else
			commit(loadMore ? LOAD_MORE : LOAD, data);
	}).catch((err) => {
		toastr.error(err.message);
	}).then(() => {
		commit(FETCHING, false);		
	});
};

export const getRow = function ({commit, state}, params) {
	 
	return window.settingService.rest("find", params);
		 
};

export const loadMoreRows = function(context) {
	return getRows(context, true);
};

export const changeSort = function(store, sort) {
	store.commit(CHANGE_SORT, sort);
	getRows(store);
};

export const changeViewMode = function(store, viewMode) {
	store.commit(CHANGE_VIEWMODE, viewMode);
	getRows(store);
};

export const saveRow = function(store, model) {
	window.settingService.rest("create", model).then((data) => {
		created(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const updateRow = function(store, model) {
	window.settingService.rest("update", model).then((data) => {
		updated(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const removeRow = function(store, model) {
	window.settingService.rest("remove", { code: model.code }).then((data) => {
		removed(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const vote = function(store, model) {
	window.settingService.rest("vote", { code: model.code }).then((data) => {
		updated(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const unVote = function(store, model) {
	window.settingService.rest("unvote", { code: model.code }).then((data) => {
		updated(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const created = function({ commit }, model) {
	commit(ADD, model);
};

export const updated = function({ commit }, model) {
	commit(UPDATE, model);
};

export const removed = function({ commit }, model) {
	commit(REMOVE, model);
};
