export function posts(state) {
	// return state.rows;
	return state.posts;
}

export function commentText(state) {
	// return state.rows;
	return state.commentText;
}

export function CommentAble(state) {
	// return state.rows;
	return state.CommentAble;
}

export function EditAble(state) {
	// return state.rows;
	return state.EditAble;
}

export function updateText(state) {
	// return state.rows;
	return state.updateText;
}

export function ReplyAble(state) {
	// return state.rows;
	return state.ReplyAble;
}

export function hasMore(state) {
	return state.hasMore;
}

// export function ptoken(state) {
// 	return state.ptoken;
// }
export function paging(state) {
	return state.paging;
}
export function fetching(state) {
	return state.fetching;
}

export function sort(state) {
	return state.sort;
}

export function viewMode(state) {
	return state.viewMode;
}
