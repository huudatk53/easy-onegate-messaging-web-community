export function SET_CONTACT(state, data) {
	state.contact = data;
}

export function SET_ORDERS(state, data) {
	state.orders = data;
}

export function SET_PRODUCTS(state, data) {
	state.products = data;
}

export function SET_SALES(state, data) {
	state.sales = data;
}
