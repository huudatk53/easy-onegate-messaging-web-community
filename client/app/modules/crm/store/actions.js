import Vue from "vue";
import toastr from "../../../core/toastr";
import Service from "../../../core/service";
import utils from "../../../core/utils";
import prod from "../../../../../server/config/prod";

export const NAMESPACE = "/api/crm";

export function resetCustomerCRM({ commit }) {
	commit("SET_CONTACT", null);
	commit("SET_ORDERS", null);
}

export function getContactCRM({ commit, state }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("getContactCRM", {
				crm_id: data.crmId,
				limit: 10
			})
			.then(data => {
				if (data.success) {
					const contact = utils.getDataCRM(data, "data.profile");
					if (contact.length > 0) {
						commit("SET_CONTACT", contact[0]);
					}
					resolve(contact[0]);
				} else {
					throw new Error(data.msg || "Kết nối tới CRM thất bại !");
				}
			})
			.catch(err => {
				Vue.prototype.$toasted.show(err.message || err , {type : 'error'})
				reject(err);
			});
	});
}

export function crmCreateContact({ commit, state }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("createContactCRM", {
				fbId: data.fbId,
				name: data.name,
				crmId: data.crmId,
				phone: data.phone,
				email: data.email,
				FriendlyUrl: data.FriendlyUrl
			})
			.then(data => {
				if (data.success) {
					resolve(data.R1.Data);
				} else {
					throw new Error(data.msg || "Kết nối tới CRM thất bại !");
				}

			})
			.catch(err => {
				Vue.prototype.$toasted.show(err.message || err , {type : 'error'})
				reject(err);
			});
	});
}

export function crmUpdateContact({ commit, state }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("updateContactCRM", data)
			.then(response => {
				if(data.Id) {
					if (response.success) {
						commit("SET_CONTACT", data);
					} else {
						throw new Error(response.msg || "Kết nối tới CRM thất bại !");
					}

					resolve();
				} else {
					if (response.success) {
						let arr = utils.getDataCRM(response, 'data.profile');
						resolve(arr);
					} else {
						throw new Error(response.msg || "Kết nối tới CRM thất bại !");
					}
				}
			})
			.catch(err => {
				Vue.prototype.$toasted.show(err.message || err , {type : 'error'})
				reject(err);
			});
	});
}

export function crmGetOrders({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("getOrdersCRM", {
				contactId: data.contactId
			})
			.then(data => {
				if (data.success) {
					const orders = utils.getDataCRM(data, "data.orders");
					commit("SET_ORDERS", orders);
					resolve();
				} else {
					throw new Error(data.msg || "Kết nối tới CRM thất bại !");
				}
			}).catch(err => {
				Vue.prototype.$toasted.show(err.message || err , {type : 'error'})
				reject(err);
			});
	});
}

export function crmCreateOrder({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("createOrderCRM", data)
			.then(data => {
				if (data.success) {
					console.log("Create order success");
					toastr.info("Thành công");
					resolve();
				} else {
					throw new Error(data.msg || "Kết nối tới CRM thất bại !");
				}
			}).catch(err => {
				Vue.prototype.$toasted.show(err.message || err , {type : 'error'})
				reject(err);
			});
	});
}

export function crmGetProducts({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("getProductsCRM", {})
			.then(data => {
				const products = utils.getDataCRM(data, "data.products");
				commit("SET_PRODUCTS", products);
				resolve();
			}).catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function crmGetSales({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.crmService
			.rest("getSalesCRM", {})
			.then(data => {
				const sales = utils.getDataCRM(data, "data.users");
				commit("SET_SALES", sales);
				resolve();
			}).catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}
