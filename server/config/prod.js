"use strict";

let pkg 	= require("../../package.json");

module.exports = {
	app: {
	},

	db: {
		uri: process.env.MONGO_URI || "mongodb://127.0.0.1:27017/" + pkg.config.dbName,
		options: {
			user: process.env.MONGO_USERNAME || "",
			pass: process.env.MONGO_PASSWORD || ""
		}
	},

	crm: {
		base: "https://yourcrmdomain/",
		username: "",
		password: ""
	}
};
