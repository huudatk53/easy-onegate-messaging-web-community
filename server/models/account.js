let config    		= require("../config");
let logger    		= require("../core/logger");
let C 				= require("../core/constants");
let fs 				= require("fs");
let path 			= require("path");

let _ 				= require("lodash");
let crypto 			= require("crypto");
let bcrypt 			= require("bcrypt-nodejs");

let db	    		= require("../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../libs/hashids")("users");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let validateLocalStrategyProperty = function(property) {
	return (this.provider !== "local" && !this.updated) || property.length;
};

let validateLocalStrategyPassword = function(password) {
	return this.provider !== "local" || (password && password.length >= 6);
};

//var Schema       = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let AccountSchema   = new Schema({
	name: {type:String, required: true},
	domain: {
		type: String,
		trim: true,
		unique: true,
		index: true,
		lowercase: true,
		required: "Please fill in a domain",
		match: [/^[a-z0-9]+$/i, "Please fill a valid domain"]
    	},
	default_contact : { type: ObjectId, ref: "User", required: true},
	stats :{
    	created_at : { type: Date},
    	updated_at: { type: Date},
    	created_by : { type: String },
    	updated_by: { type: String },
    	deleted: {type:Boolean, default: false}
	}
});

AccountSchema.pre("save", function(next) {
	let currentDate = new Date();
	this.stats.updated_at = currentDate;
	this.stats.created_by = "1";
	this.stats.updated_by = "1";
	if (!this.stats.created_at)
		this.stats.created_at = currentDate;
	next();
});


/**
 * Virtual `code` field instead of _id
 */
AccountSchema.virtual("code").get(function() {
	return this.encodeID();
});

/**
 * Auto increment for `_id`
 */
// AccountSchema.plugin(autoIncrement.plugin, {
// 	model: "Accounts",git
// 	startAt: 1
// });


/**
 * Encode `_id` to `code`
 */
AccountSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

/**
 * Decode `code` to `_id`
 */
AccountSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

mongoose.model("Account", AccountSchema);
module.exports = mongoose.models.Account;
