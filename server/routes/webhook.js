"use strict";
let config = require("../config");
let logger = require("../core/logger");
let images = require("../libs/pics");
let request = require("request");
let services = require("../core/services");
let context = require("../core/context");
let bull = require("../core/hook-bull");
let _ = require("lodash");
var winston = require('winston');
const winstonRotator = require('winston-daily-rotate-file');
let mkdirp = require("mkdirp");
let path = require("path");
let fs = require("fs");
module.exports = function(app, db) {
	// Creates the endpoint for our webhook
	let messageService = services.get("messages");
	let ctx = context.CreateToServiceInit(messageService);
	let facebookService = services.get("facebook");
	let notificationService = services.get("notifications");

	let hookServices = {
		conversationsService:services.get("conversations"),
		feedService:services.get("feeds"),
		messageService:services.get("messages"),
	};

	let logDir = "log";
	if (!fs.existsSync(logDir)) {
		try {
			mkdirp(logDir);
		} catch (e) {
				console.log("Cannotcreatefolder")
		}
	}

	const logges = new (winston.Logger)({
	  level: 'info',
	  json: true,
	  transports: [
	    new (winston.transports.DailyRotateFile)({ filename: 'log/webhook.log', datePattern: 'yyyy-MM-ddTHH.', prepend: true,name: 'file.info', level: 'info' }),
	  ]
	});
	// if (process.env.NODE_ENV !== 'production') {
	//   logges.add(new winston.transports.Console({
	//     format: winston.format.simple()
	//   }));
	// }
	app.post("/webhook", (req, res) => {
		let body = req.body;
		logger.debug("++++ body start ++++");
		// logger.debug(body);
		logger.debug("++++ body end ++++");
		res.status(200).send("EVENT_RECEIVED");
		bull.add(body)
		try {
			logges.info({body});
		} catch (e) {
			console.log("Cannotlogfile")
		}
		if (body.object === "page") {
			body.entry.forEach(function(pageEntry) {
				notificationService.facebookHook(pageEntry);
				if (pageEntry.changes) {
					pageEntry.changes.forEach(change => {
						if (change.field) {
							hookServices[change.field+"Service"].handleWebhook(change, pageEntry.id, context);
						}
					});
				}
				else if (pageEntry.messaging) {
					pageEntry.messaging.forEach(messagingEvent => {
						if(messagingEvent.message && (messagingEvent.message.text || messagingEvent.message.attachments)){
								hookServices.conversationsService.handleWebhook(messagingEvent, pageEntry.id, context);
								hookServices.messageService.handleWebhook(messagingEvent, pageEntry.id, context);
						}
					});
				}
			});

		} else {
			res.sendStatus(404);
		}
	});

	// Adds support for GET requests to our webhook
	app.get("/webhook", (req, res) => {
		//res.sendStatus(200);
		// Your verify token. Should be a random string.
		//let VERIFY_TOKEN = "0902318580";
		let VERIFY_TOKEN = config.authKeys.facebook.webhookVerifyToken;

		// Parse the query params
		let mode = req.query["hub.mode"];
		let token = req.query["hub.verify_token"];
		let challenge = req.query["hub.challenge"];

		// Checks if a token and mode is in the query string of the request
		if (mode && token) {
			// Checks the mode and token sent is correct
			if (mode === "subscribe" && token === VERIFY_TOKEN) {
				// Responds with the challenge token from the request
				console.log("WEBHOOK_VERIFIED");
				//handleMessage('WEBHOOK_VERIFIED', {text: 'heello'});
				res.status(200).send(challenge);
			} else {
				// Responds with '403 Forbidden' if verify tokens do not match
				res.sendStatus(403);
			}
		}
	});
};
