"use strict";

const {Storage} = require("@google-cloud/storage"); 
const config = {
	apiKey: "AIzaSyC27QgTpSc2JsjwCV9VQWVD7hLaNj9eU30",
	authDomain: "vuejs-chat-firebase.firebaseapp.com",
	databaseURL: "https://vuejs-chat-firebase.firebaseio.com",
	projectId: "vuejs-chat-firebase",
	storageBucket: "vuejs-chat-firebase.appspot.com",
	messagingSenderId: "654191283502"
};
const storage = new Storage({
	projectId: "vuejs-chat-firebase",
	keyFilename: "server/core/vuejs-chat-firebase-ab81a4a1a150.json"
});
module.exports = { 
	storage,  
	config
};