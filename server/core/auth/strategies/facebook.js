"use strict";

let config 	= require("../../../config");
let helper 	= require("../helper");
let _ = require("lodash");
let passport 			= require("passport");
let FacebookStrategy  	= require("passport-facebook").Strategy;
let FB = require("fb");
let FbToken		= require("../../../models/fbtoken");
let User        = require("../../../models/user");
module.exports = function(locals) {
	if (config.authKeys.facebook.clientID && config.authKeys.facebook.clientSecret) {

		passport.use("facebook", new FacebookStrategy({
			clientID: config.authKeys.facebook.clientID,
			clientSecret: config.authKeys.facebook.clientSecret,
			callbackURL: config.authKeys.facebook.callbackURL,
			profileFields: ["name", "email", "link", "locale", "timezone"],
			passReqToCallback: true
		}, function(req, accessToken, refreshToken, profile, done) {
			FB.api("oauth/access_token", {
				grant_type:"fb_exchange_token",
				client_id: config.authKeys.facebook.clientID,
				client_secret: config.authKeys.facebook.clientSecret,
				fb_exchange_token: accessToken,
			}, function (res) {
				if(!res || res.error) {
					console.log(!res ? "error occurred" : res.error);
					return;
				}
				FB.setAccessToken(res.access_token);
				FB.api("me", "GET", {fields:"accounts.limit(1000){id, access_token, tasks}"}, (response)=> {
					let fb_pageIds = [];
					let fb_pages_tasks = {};
					if (response.accounts && response.accounts.data) {

						for (let i = 0; i<response.accounts.data.length; i++){
							let p = response.accounts.data[i];
							fb_pageIds.push(p.id);
							fb_pages_tasks[p.id] = p.tasks;
						}

						_.forIn(response.accounts.data, (object) => {
							locals.pageTokenCache.set(profile.id + "_"+object.id, object.access_token);
							let pTokenModel = {
								type: "p",
								userId: profile.id,
								key: profile.id + "_"+object.id,
								token: object.access_token,
							};
							FbToken.findOneAndUpdate(
								{key: pTokenModel.key, type: pTokenModel.type}, // find a document with that filter
								pTokenModel, // document to insert when nothing was found
								{upsert: true, new: true, runValidators: true}
							).then(data=>{
							}).catch(err=>{
							});

						});

						locals.userTokenCache.set(profile.id, res.access_token);
						let uTokenModel = {
							type: "u",
							userId: profile.id,
							key: profile.id,
							token: res.access_token,
						};
						FbToken.findOneAndUpdate(
							{key: uTokenModel.key, type: uTokenModel.type}, // find a document with that filter
							uTokenModel, // document to insert when nothing was found
							{upsert: true, new: true, runValidators: true}
						).then(data=>{
						}).catch(err=>{
						});

					}
					helper.linkToSocialAccount({
						req,
						accessToken,
						refreshToken,
						profile,
						done,
						fb_pageIds: fb_pageIds,
						fb_pages_tasks: fb_pages_tasks,
						provider: "facebook",
						email: profile._json.email,
						userData: {
							name: profile.name.givenName + " " + profile.name.familyName,
							gender: profile._json.gender,
							picture: `https://graph.facebook.com/${profile.id}/picture?type=large`,
							location: (profile._json.location) ? profile._json.location.name : null,
						}
					});

				});


			});
		}));

	}
};
