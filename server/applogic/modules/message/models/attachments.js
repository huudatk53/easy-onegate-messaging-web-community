"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("attachments");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let AttachmentSchema = new Schema({
	name:{
		type: String
	},
	type: {
		type: String
	},
	path: {
		type: String
	},
	url: {
		type: String
	},
	downloads:[{
		type: String
	}],
	fb_id: {
		type: String,
		trim: true,
		unique: true,
		sparse: true
	},
	fb_page :{
		type: String,
		trim: true
	},
	conversation_id: {
		type: String
	},
	permalink_url: {
		type: String
	},

	inserted_at: {
		type: Date
	},
	is_hidden: {
	 type: Boolean
	},
	is_favorite: {
	 type: Boolean
	},
	folder_ids: {
		type: Array,
		// ref: "Folder"
	},
	is_parent : {
		type: Boolean
	   },
	is_parent_hidden : {
		type: Boolean
	   },
	is_removed : {
		type: Boolean,
		default: false
	   },
	verb : {
		type: String
	},
	like_count : {
		type: Number
	},
	author: {
		type: String,
		//required: "Please fill in an author ID",
		ref: "User"
	},
	edited_by: {
		type: String,
		//required: "Please fill in an author ID",
		ref: "User"
	},
	views: {
		type: Number,
		default: 0
	},
	voters: [{
		type: String,
		ref: "User"
	}],
	votes: {
		type: Number,
		default: 0
	},
	editedAt: {
		type: Date
	},
	metadata: {}

}, schemaOptions);

AttachmentSchema.virtual("code").get(function() {
	return this.encodeID();
});

// MessageSchema.plugin(autoIncrement.plugin, {
// 	model: "Message",
// 	startAt: 1
// });

AttachmentSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

AttachmentSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};
AttachmentSchema.virtual("downloaders", {
	ref: "User", // The model to use
	localField: "downloads", // Find people where `localField`
	foreignField: "_id",
	justOne: false // is equal to `foreignField`
});
/*
MessageSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Attachment = mongoose.model("Attachment", AttachmentSchema);

module.exports = Attachment;
