"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("conversations");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let TYPES 		= require("../type");
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},	 
	discriminatorKey: "kind"
};

let ChatSchema = new Schema({
	// _id: {
	// 	type: String,
	// 	trim: true,
	// 	unique: true,
	// 	required: "Please fill in a conversation ID",	
	// },
	fb_id: {
		type: String,
		trim: true,
		unique: true		 
	},
	
	parent_id: {
		type: String
	},
	 
	fb_page:{
		type: String, ref:"Page", field:"fb_id"
	},
	link : {
		type: String
	}, 
	updated_time : {
		type: Date
	},
	snippet: {
		type:String
	},
	message_count: {
		type: Number,
		default: 0
	},
	participants: {
		type: Array
	},
	senders: {
		type: Array
	},

	can_reply:{
		type: Boolean
	},
	is_subscribed:{
		type: Boolean
	},
	unread_count: {
		type: Number,
		default: 0
	},
	phone_count: {
		type: Number,
		default: 0
	}
	// title: {
	// 	type: String,
	// 	trim: true
	// },
	// content: {
	// 	type: String,
	// 	trim: true
	// },
	// author: {
	// 	type: String,
	// 	required: "Please fill in an author ID",
	// 	ref: "User"
	// },
	// views: {
	// 	type: Number,
	// 	default: 0
	// },
	// voters: [{
	// 	type: String,
	// 	ref: "User"
	// }],
	// votes: {
	// 	type: Number,
	// 	default: 0
	// },
	// editedAt: {
	// 	type: Date
	// },
	// metadata: {}

}, schemaOptions);

ChatSchema.virtual("code").get(function() {
	return this.encodeID();
});

 



// ChatSchema.plugin(autoIncrement.plugin, {
// 	model: "Conversation",
// 	startAt: 1
// });

ChatSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

ChatSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};


/*
PostSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Chat= mongoose.model("Chat", ChatSchema);

module.exports = Chat;
