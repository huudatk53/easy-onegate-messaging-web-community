"use strict";
const moment = require("moment");

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");
let mongoose = require("mongoose");
let CONTEXT = require("../../../core/context");

const Contact = require("../contacts/models/contact");
const Message = require("../comment/models/comment")("Message");
const Comment = require("../comment/models/comment")("Comment");
const User = require("../people/models/user");
const Conversation = require("../conversations/models/conversation");
const Statistic = require("./models/statistic");
const Setting = require("../settings/models/setting");
const Tag = require("../tags/models/tags");

const TIME_SEGMENTS = {
	today: (gmt) => {
		const t = moment().tz(gmt);
		return [t.clone().startOf("day"), t.clone().endOf("day")];
	},
	yesterday: (gmt) => {
		const t = moment().tz(gmt).subtract(1, "days");
		return [t.clone().startOf("day"), t.clone().endOf("day")];
	},
	thisWeek: (gmt) => {
		const t = moment().tz(gmt).startOf("week");
		return [t.clone().startOf("week"), t.clone().endOf("week")];
	},
	lastWeek: (gmt) => {
		const t = moment().tz(gmt).subtract(1, "week");
		return [t.clone().startOf("week"), t.clone().endOf("week")];
	},
	thisMonth: (gmt) => {
		const t = moment().tz(gmt).startOf("month");
		return [t.clone().startOf("month"), t.clone().endOf("month")];
	},
	lastMonth: (gmt) => {
		const t =  moment().tz(gmt).subtract(1, "months");
		return [t.clone().startOf("month"), t.clone().endOf("month")];
	}
};

module.exports = {
	settings: {
		name: "statistics",
		version: 1,
		namespace: "statistics",
		rest: true,
		ws: false,
		els: false,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Statistic
	},

	actions: {
		/**
		 * Get overview data
		 */
		getOverviewData: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;
					const contactPromises = [];
					const messagePromises = [];

					Object.keys(TIME_SEGMENTS).forEach(k => {
						const times = TIME_SEGMENTS[k](gmt);
						const contactQuery = {
							fb_pages: {
								$in: pageIds
							},
							createdAt: {
								$gte: times[0].toDate(),
								$lte:times[1].toDate()
							}
						};
						const messageQuery = {
							fb_page: {
								$in: pageIds
							},
							created_time: {
								$gte: times[0].toDate(),
								$lte:times[1].toDate()
							}
						};

						contactPromises.push(Contact.count(contactQuery));
						messagePromises.push(Message.count(messageQuery));
					});

					const contactOverview = await Promise.all(contactPromises);
					const messageOverview = await Promise.all(messagePromises);
					const overview = {};

					Object.keys(TIME_SEGMENTS).forEach((k, i) => {
						Object.assign(overview, {
							[k]: {
								contact: contactOverview[i],
								message: messageOverview[i]
							}
						});
					});

					return overview;
				} catch (err) {
					throw err;
				}
			}
		},

		/**
		 * Get contact & message in last 2 month
		 */
		getContactAndMessageRecently: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;

					const contactPromises = [];
					const messagePromises = [];
					const labels = [];

					let date = moment().tz(gmt).subtract(1, "month").startOf("month");
					let today = moment().tz(gmt).add(1, "day").startOf("day");

					while (date.isBefore(today)) {
						labels.push(date.clone().format("DD-MM-YYYY"));
						const contactQuery = {
							fb_pages: { $in: pageIds },
							createdAt: {
								$gte: date.clone().toDate(),
								$lte:date.clone().endOf("day").toDate()
							}
						};
						const messageQuery = {
							fb_page: {
								$in: pageIds
							},
							created_time: {
								$gte: date.clone().toDate(),
								$lte:date.clone().endOf("day").toDate()
							}
						};
						contactPromises.push(Contact.count(contactQuery));
						messagePromises.push(Message.count(messageQuery));
						date.add(1, "day");
					}

					const contactCounts = await Promise.all(contactPromises);
					const messageCounts = await Promise.all(messagePromises);


					return {
						labels,
						contactCounts,
						messageCounts
					};
				} catch (error) {
					throw error;
				}
			}
		},

		/**
		 * Count message & comment by date
		 */
		getMessageAndCommentByDate: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;

					const start = moment().tz(gmt).startOf("day");
					const end = moment().tz(gmt).endOf("day");

					const messagePromises = [];
					const commentPromises = [];
					const labels = [];

					while (start.isBefore(end)) {
						const endTime = start.clone().endOf("hour");
						const query = {
							fb_page: {
								$in: pageIds
							},
							created_time: {
								$gte: start.clone().toDate(),
								$lte: endTime.toDate()
							}
						};
						messagePromises.push(Message.count(query));
						commentPromises.push(Comment.count(query));
						labels.push(start.clone().format("HH:mm"));
						start.add(1, "hour");
					}

					const messageCounts = await Promise.all(messagePromises);
					const commentCounts = await Promise.all(commentPromises);

					const messageAndCommentCounts = messageCounts.map((messageCount, index) => {
						return messageCount + commentCounts[index];
					});

					return {
						labels,
						messageAndCommentCounts
					};
				} catch (error) {
					throw error;
				}
			}
		},

		/**
		 * Top user by interactive with customer
		 */
		getTopUserInteraction: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;
					const limit = ctx.req.body.limit || 4;

					const start = moment().tz(gmt).startOf("day");
					const end = moment().tz(gmt).endOf("day");

					const users = await User.find().lean();

					const query = {
						fb_page: {
							$in: pageIds
						},
						created_time: {
							$gte: start.clone().toDate(),
							$lte: end.clone().toDate()
						}
					};

					const commentPromises = [];
					const messagePromises = [];

					users.forEach(user => {
						const finalQuery = Object.assign({
							"created_by.fb_id": user.socialLinks.facebook
						}, query);

						commentPromises.push(Comment.count(finalQuery));
						messagePromises.push(Message.count(finalQuery));
					});

					const commentCounts = await Promise.all(commentPromises);
					const messageCounts = await Promise.all(messagePromises);

					let userInteractions = users.map((user, index) => {
						return {
							user: _.pick(user, ["profile", "socialLinks"]),
							commentCounts: commentCounts[index],
							messageCounts: messageCounts[index],
							total: commentCounts[index] + messageCounts[index]
						};
					});

					userInteractions = userInteractions.filter(val => val.total > 0);
					userInteractions = userInteractions.sort((a, b) => a.total - b.total);
					userInteractions = userInteractions.slice(0, limit);

					return userInteractions;
				} catch (error) {
					throw error;
				}
			}
		},

		getCustomerInteractionRecently: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;

					const start = moment().tz(gmt).subtract(5, "month").startOf("month");
					const end = moment().tz(gmt).endOf("month");

					const commentPromises = [];
					const messagePromises = [];
					const labels = [];

					while (start.isBefore(end)) {
						const query = {
							fb_page: {
								$in: pageIds
							},
							created_time: {
								$gte: start.clone().toDate(),
								$lte: start.clone().endOf("month").toDate()
							}
						};

						labels.push(start.clone().format("MM-YYYY"));
						commentPromises.push(Comment.count(query));
						messagePromises.push(Message.count(query));

						start.add(1, "month");
					}

					const commentCounts = await Promise.all(commentPromises);
					const messageCounts = await Promise.all(messagePromises);

					return {
						labels,
						commentCounts,
						messageCounts
					};
				} catch (error) {
					throw error;
				}
			}
		},

		getNewStatistics: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;

					const end = moment().tz(gmt).add(1, "month").startOf("month");
					const start = moment().tz(gmt).startOf("year");

					const query = {
						phone: {
							model: [Message, Comment],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									},
									phones: {
										$exists: true
									}
								};
							}
						},
						message: {
							model: [Message, Comment],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									}
								};
							}
						},
						oldContactInteraction: {
							model: [Contact],
							query: (s, e) => {
								return {
									fb_pages: { $in: pageIds },
									lastCommunication: {
										$gte: s,
										$lte: e
									},
									createdAt: {
										$lt: s
									}
								};
							}
						},
						newContact: {
							model: [Contact],
							query: (s, e) => {
								return {
									fb_pages: { $in: pageIds },
									createdAt: {
										$gte: s,
										$lte: e
									}
								};							}
						},
						conversations: {
							model: [Conversation],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									createdAt: {
										$gte: s,
										$lte: e
									}
								};
							}
						}
					};

					const segments = [];
					while (start.isBefore(end)) {
						const s = start.clone().startOf("month");
						const e = start.clone().endOf("month");
						segments.push([
							s,
							e
						]);
						start.add(1, "month");
					}

					const result = {
						phone: [],
						message: [],
						oldContactInteraction: [],
						newContact: [],
						labels: [],
						conversations: []
					};

					for (const time of segments) {
						result.labels.push(
							time[0].clone().format("MM-YYYY")
						);

						for (const key of Object.keys(query)) {
							let total = 0;
							for (const Model of query[key].model) {
								const count = await Model.count(query[key].query(time[0], time[1]));
								total += count;
							}
							result[key].push(total);
						}
					}

					return result;
				} catch (err) {
					throw err;
				}
			}
		},

		getPageStatisticsByDate: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const gmt = ctx.req.body.gmt;
					const from = ctx.req.body.from ? moment(ctx.req.body.from) : moment().tz(gmt).subtract(30, "days");
					const to = ctx.req.body.to ? moment(ctx.req.body.to) : moment().tz(gmt);

					const start = from.startOf("day");
					const end = to.endOf("day");

					const query = {
						phones: {
							model: [Message, Comment],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									},
									phones: {
										$exists: true
									}
								};
							}
						},
						customerInbox: {
							model: [Message],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									},
									"from.id": {
										$nin: pageIds
									}
								};
							}
						},
						pageInbox: {
							model: [Message],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									},
									"from.id": {
										$in: pageIds
									}
								};
							}
						},
						customerComments: {
							model: [Comment],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									},
									"from.id": {
										$nin: pageIds
									}
								};
							}
						},
						pageComments: {
							model: [Comment],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: s.toDate(),
										$lte: e.toDate()
									},
									"from.id": {
										$in: pageIds
									}
								};
							}
						},
						oldContacts: {
							model: [Contact],
							query: (s, e) => {
								return {
									fb_pages: { $in: pageIds },
									lastCommunication: {
										$gte: s,
										$lte: e
									},
									createdAt: {
										$lt: s
									}
								};
							}
						},
						contacts: {
							model: [Contact],
							query: (s, e) => {
								return {
									fb_pages: { $in: pageIds },
									createdAt: {
										$gte: s,
										$lte: e
									}
								};
							}
						},
						conversations: {
							model: [Conversation],
							query: (s, e) => {
								return {
									fb_page: {
										$in: pageIds
									},
									createdAt: {
										$gte: s,
										$lte: e
									}
								};
							}
						}
					};

					const segments = [];
					while (start.isBefore(end)) {
						const s = start.clone().startOf("day");
						const e = start.clone().endOf("day");
						segments.push([
							s,
							e
						]);
						start.add(1, "day");
					}

					const result = {
						phones: [],
						customerComments: [],
						pageComments: [],
						customerInbox: [],
						pageInbox: [],
						customerMessages: [],
						pageMessages: [],
						oldContacts: [],
						contacts: [],
						labels: [],
						conversations: []
					};

					let i = 0;
					for (const time of segments) {
						result.labels.push(
							time[0].clone().format("DD-MM-YYYY")
						);

						for (const key of Object.keys(query)) {
							let total = 0;
							for (const Model of query[key].model) {
								const count = await Model.count(query[key].query(time[0], time[1]));
								total += count;
							}
							result[key].push(total);
						}

						result.customerMessages.push(result.customerInbox[i] + result.customerComments[i]);
						result.pageMessages.push(result.pageInbox[i] + result.pageComments[i]);

						i++;
					}

					return result;
				} catch (err) {
					throw err;
				}
			}
		},

		getStaffs: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const staffs = await User
						.find({
							fb_pageIds: {
								$in: pageIds
							}
						})
						.lean();

					return staffs;
				} catch (error) {
					throw error;
				}
			}
		},

		getStaffStatistics: {
			cache: false,
			handler: async (ctx) => {
				try {
					const pageIds = ctx.req.body.pageIds;
					const staffIds = ctx.req.body.staffIds;
					const gmt = ctx.req.body.gmt || "Asia/Ho_Chi_Minh";
					const from = ctx.req.body.from ? moment(ctx.req.body.from) : moment().tz(gmt).subtract(30, "days");
					const to = ctx.req.body.to ? moment(ctx.req.body.to) : moment().tz(gmt);

					const start = from.startOf("day");
					const end = to.endOf("day");

					const result = {
						labels: [],
						list: []
					};

					const segments = [];
					while (start.isBefore(end)) {
						const s = start.clone().startOf("day");
						const e = start.clone().endOf("day");
						segments.push([
							s,
							e
						]);
						start.add(1, "day");
					}

					for (const time of segments) {
						result.labels.unshift(
							time[0].clone().format("DD-MM-YYYY")
						);
						const staff = [];
						const staffPromises = [];

						for (const staffId of staffIds) {
							const promises = [
								Statistic.count({
									type: "message",
									fb_page_id: {
										$in: pageIds
									},
									fb_created_time: {
										$gte: time[0].clone().toDate(),
										$lte: time[1].clone().toDate()
									},
									user_id: staffId
								}),
								Statistic.count({
									type: "comment",
									fb_page_id: {
										$in: pageIds
									},
									fb_created_time: {
										$gte: time[0].clone().toDate(),
										$lte: time[1].clone().toDate()
									},
									user_id: staffId
								}),
								Message.count({
									fb_page: {
										$in: pageIds
									},
									created_time: {
										$gte: time[0].clone().toDate(),
										$lte: time[1].clone().toDate()
									},
									phones: {
										$exists: true
									},
									author: staffId
								})
							];

							staffPromises.push(Promise.all(promises));
						}
						const promiseResults = await Promise.all(staffPromises);

						for (let i = 0; i < staffIds.length; i++) {
							staff.push({
								staffId: staffIds[i],
								inbox: promiseResults[i][0],
								comment: promiseResults[i][1],
								phone: promiseResults[i][2]
							});
						}

						result.list.unshift(staff);
					}

					return result;
				} catch (error) {
					throw error;
				}
			}
		},

		getInteractionStatistics: {
			cache: false,
			handler: async (ctx) => {
				const pageIds = ctx.req.body.pageIds;
				const staffIds = ctx.req.body.staffIds;
				const gmt = ctx.req.body.gmt;
				const from = ctx.req.body.from ? moment(ctx.req.body.from) : moment().tz(gmt).subtract(30, "days");
				const to = ctx.req.body.to ? moment(ctx.req.body.to) : moment().tz(gmt);

				const start = from.startOf("day");
				const end = to.endOf("day");

				const segments = [];
				while (start.isBefore(end)) {
					const s = start.clone().startOf("day");
					const e = start.clone().endOf("day");
					segments.push([
						s,
						e
					]);
					start.add(1, "day");
				}

				const result = {
					labels: [],
					list: []
				};

				for (const time of segments) {
					result.labels.unshift(
						time[0].clone().format("DD-MM-YYYY")
					);

					const staffPromises = [];
					const staff = [];
					for (const staffId of staffIds) {
						const inboxFbContactIds = await Statistic.distinct("fb_contact_id", {
							type: "message",
							fb_page_id: {
								$in: pageIds
							},
							fb_created_time: {
								$gte: time[0].clone().toDate(),
								$lte: time[1].clone().toDate()
							},
							user_id: staffId
						});

						const commentFbContactIds = await Statistic.distinct("fb_contact_id", {
							type: "comment",
							fb_page_id: {
								$in: pageIds
							},
							fb_created_time: {
								$gte: time[0].clone().toDate(),
								$lte: time[1].clone().toDate()
							},
							user_id: staffId
						});

						const fbContactIds = _.uniq(_.concat(inboxFbContactIds, commentFbContactIds));

						const oldFbContactIds = await Statistic.distinct("fb_contact_id", {
							fb_contact_id: {
								$in: fbContactIds
							},
							fb_page_id: {
								$in: pageIds
							},
							fb_created_time: {
								$lt: time[0].clone().toDate()
							},
							user_id: staffId
						});

						const inbox = inboxFbContactIds.length;
						const comment = commentFbContactIds.length;
						const total = fbContactIds.length;
						const oldContact = oldFbContactIds.length;
						const contact = total - oldContact;

						staff.push({
							staffId,
							inbox,
							comment,
							contact,
							oldContact,
							total
						});
					}

					result.list.unshift(staff);
				}

				return result;
			}
		},

		getTotalInteractionStatistics: {
			cache: false,
			handler: async (ctx) => {
				const pageIds = ctx.req.body.pageIds;
				const staffIds = ctx.req.body.staffIds;
				const gmt = ctx.req.body.gmt;
				const from = ctx.req.body.from ? moment(ctx.req.body.from) : moment().tz(gmt).subtract(30, "days");
				const to = ctx.req.body.to ? moment(ctx.req.body.to) : moment().tz(gmt);

				const start = from.startOf("day");
				const end = to.endOf("day");

				const result = [];
				for (const staffId of staffIds) {
					const inboxFbContactIds = await Statistic.distinct("fb_contact_id", {
						type: "message",
						fb_page_id: {
							$in: pageIds
						},
						fb_created_time: {
							$gte: start.clone().toDate(),
							$lte: end.clone().toDate()
						},
						user_id: staffId
					});

					const commentFbContactIds = await Statistic.distinct("fb_contact_id", {
						type: "comment",
						fb_page_id: {
							$in: pageIds
						},
						fb_created_time: {
							$gte: start.clone().toDate(),
							$lte: end.clone().toDate()
						},
						user_id: staffId
					});

					const fbContactIds = _.uniq(_.concat(inboxFbContactIds, commentFbContactIds));

					const oldFbContactIds = await Statistic.distinct("fb_contact_id", {
						fb_contact_id: {
							$in: fbContactIds
						},
						fb_page_id: {
							$in: pageIds
						},
						fb_created_time: {
							$lt: start.clone().toDate()
						},
						user_id: staffId
					});

					const inbox = inboxFbContactIds.length;
					const comment = commentFbContactIds.length;
					const total = fbContactIds.length;
					const oldContact = oldFbContactIds.length;
					const contact = total - oldContact;

					result.push({
						staffId,
						inbox,
						comment,
						contact,
						oldContact,
						total
					});
				}

				return result;
			}
		},

		getTags: {
			cache: false,
			handler: async(ctx) => {
				const pageIds = ctx.req.body.pageIds;
				const tagSetting = await Setting
					.findOne({
						type: "tagsSetting",
						fb_id: {
							$in: pageIds
						}
					})
					.lean();

				return tagSetting.tags;
			}
		},

		getTagStatistics: {
			cache: false,
			handler: async(ctx) => {
				const pageIds = ctx.req.body.pageIds;
				const tagCodes = ctx.req.body.tagCodes;
				const gmt = ctx.req.body.gmt;
				const unit = ctx.req.body.unit;
				const from = ctx.req.body.from ? moment(ctx.req.body.from).tz(gmt) : moment().tz(gmt).subtract(30, "days");
				const to = ctx.req.body.to ? moment(ctx.req.body.to).tz(gmt) : moment().tz(gmt);

				const start = from.startOf("day");
				const end = to.endOf("day");

				const timeFormat = {
					day: "DD-MM",
					hour: "hh:mm"
				};

				const segments = [];
				while (start.isBefore(end)) {
					const s = start.clone().startOf(unit);
					const e = start.clone().endOf(unit);
					segments.push([
						s,
						e
					]);
					start.add(1, unit);
				}

				const result = {
					labels: [],
					list: []
				};

				const promises = [];
				for (const time of segments) {
					const timePromises = [];
					result.labels.unshift(time[0].clone().format(timeFormat[unit]));
					for (const tagCode of tagCodes) {
						console.log({
							tagcode: tagCode,
							fb_page: {
								$in: pageIds
							},
							createdAt: {
								$gte: time[0].clone().toDate(),
								$lte: time[1].clone().toDate()
							}
						});
						timePromises.push(
							Tag.count({
								tagcode: tagCode,
								fb_page: {
									$in: pageIds
								},
								createdAt: {
									$gte: time[0].clone().toDate(),
									$lte: time[1].clone().toDate()
								}
							})
						);
					}

					promises.push(Promise.all(timePromises));
				}

				const data = await Promise.all(promises);

				for (let i = 0; i < segments.length; i++) {
					const timeStatistic = [];
					for (let j = 0; j < tagCodes.length; j++) {
						timeStatistic.push({
							tagCode: tagCodes[j],
							count: data[i][j]
						});
					}
					result.list.unshift(timeStatistic);
				}

				return result;
			}
		}
	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		search() {
			let filter = {};
			return this.collection.find().exec().then( (docs) => {
				return this.toJSON(docs);
			}).then(json => {
				return this.populateModels(json);
			});
		},
		updateCount(ctx, params) {
			// ctx.assertModelIsExist(ctx.t("app:StatisticNotFound"));
			// this.validateParams(ctx);
			let model = {};
			// if (params.name != null)
			// 	model.name = params.name;

			return this.collection.findOneAndUpdate({fb_id: params.fb_id}, params, { new: false}).exec()
			.then((doc) => {
				return this.toJSON(doc);
			});
			// .then((json) => {
			// 	return this.populateModels(json);
			// })
			// .then((json) => {
			// 	this.notifyModelChanges(ctx, "updated", json);
			// 	return json;
			// });
		},
		createCount(ctx, params) {
			// this.validateParams(ctx, true);

			let tag = new Statistic({
				sent_by_page: params.sent_by_page,
				fb_contact_id: params.fb_contact_id,
				fb_conversation_id: params.fb_conversation_id,
				type: params.type,
				fb_page_id: params.fb_page_id,
				fb_id: params.fb_id,
				user_id: params.user_id,
				hour: params.created_time.getHours(),
				date: params.created_time.getDate(),
				month: params.created_time.getMonth(),
				year: params.created_time.getFullYear(),
				fb_created_time: params.created_time,
				phone_count: params.phone_count
			});

			return tag.save()
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			});
		},
		removeCount(ctx, params) {
			// ctx.assertModelIsExist(ctx.t("app:StatisticNotFound"));

			return Statistic.remove(params)
			.then((data) => {
				return data;
			});
			// .then((json) => {
			// 	// this.notifyModelChanges(ctx, "removed", json);
			// 	return json;
			// });
		}
	},

	init(ctx) {
		// Fired when start the service
	}
};
