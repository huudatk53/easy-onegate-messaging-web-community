"use strict";

let logger 		= require("../../../core/logger");
let config 		= require("../../../config");
let C 	 		= require("../../../core/constants");

let _			= require("lodash");

let Tags 		= require("./models/tags");

module.exports = {
	settings: {
		name: "tags",
		version: 1,
		namespace: "tags",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Tags,

		modelPropFilter: "code name tagcode color fb_page fb_id createdAt created_by"
	},

	actions: {
		find: {
			cache: false,
			handler(ctx) {
				let filter = {};
				if (ctx.params.condition)
					filter = _.assign(filter, ctx.params.condition);
				let query = Tags.find(filter);
				return ctx.queryPageSort(query).exec().then( (docs) => {
					return this.toJSON(docs);
				}).then(json => {
					return this.populateModels(json);
				});
			}
		},

		// return a model by ID
		get: {
			cache: false,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:TagsNotFound"));
				return Promise.resolve(ctx.model);
			}
		},

		create(ctx) {
			this.validateParams(ctx, true);

			let tag = new Tags({
				name: ctx.params.name,
				tagcode: ctx.params.code,
				color: ctx.params.color,
				fb_page: ctx.params.fb_page,
				fb_id: ctx.params.fb_id,
				created_by: ctx.user
			});

			return tag.save()
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "created", json);
				return json;
			});
		},

		update(ctx) {
			ctx.assertModelIsExist(ctx.t("app:TagsNotFound"));
			this.validateParams(ctx);
			var model = {}
			if (ctx.params.name != null)
				model.name = ctx.params.name;

			if (ctx.params.imageCount != null) {
				model.$inc = { imageCount: ctx.params.imageCount };
				// doc.imageCount = doc.imageCount + ctx.params.imageCount
			}
			return this.collection.findByIdAndUpdate(ctx.modelID, model, { new: true}).exec()
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "updated", json);
				return json;
			});
		},

		remove(ctx) {
			ctx.assertModelIsExist(ctx.t("app:TagsNotFound"));

			return Tags.remove({ _id: ctx.modelID })
			.then(() => {
				return ctx.model;
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "removed", json);
				return json;
			});
		}

	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("name"))
				ctx.validateParam("name").trim().notEmpty(ctx.t("app:TagsNameCannotBeBlank")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		},
		createCount(ctx, params) {
			// this.validateParams(ctx, true);

			let tag = new Tags({
				name: params.name,
				tagcode: params.code,
				color: params.color,
				fb_page: params.fb_page,
				fb_id: params.fb_id,
				created_by_id: ctx.user.id
			});

			return tag.save()
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
		},
		removeCount(ctx, params) {
			// ctx.assertModelIsExist(ctx.t("app:TagsNotFound"));

			return Tags.remove(params)
			.then((data) => {
				return data;
			})
			// .then((json) => {
			// 	// this.notifyModelChanges(ctx, "removed", json);
			// 	return json;
			// });
		}
	},

	init(ctx) {
		// Fired when start the service
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
			tags(limit: Int, offset: Int, sort: String): [Tags]
			tag(code: String): Tags
		`,

		types: `
			type Tags {
				code: String!
				address: String
				type: String
				name: String
				description: String
				status: Int
				lastCommunication: Timestamp
			}
		`,

		mutation: `
			tagCreate(name: String!, address: String, type: String, description: String, status: Int): Tags
			tagUpdate(code: String!, name: String, address: String, type: String, description: String, status: Int): Tags
			tagRemove(code: String!): Tags
		`,

		resolvers: {
			Query: {
				tags: "find",
				tag: "get"
			},

			Mutation: {
				tagCreate: "create",
				tagUpdate: "update",
				tagRemove: "remove"
			}
		}
	}

};

/*
## GraphiQL test ##

# Find all tags
query getTagss {
  tags(sort: "lastCommunication", limit: 5) {
    ...tagFields
  }
}

# Create a new tag
mutation createTags {
  tagCreate(name: "New tag", address: "192.168.0.1", type: "raspberry", description: "My tag", status: 1) {
    ...tagFields
  }
}

# Get a tag
query getTags($code: String!) {
  tag(code: $code) {
    ...tagFields
  }
}

# Update an existing tag
mutation updateTags($code: String!) {
  tagUpdate(code: $code, address: "127.0.0.1") {
    ...tagFields
  }
}

# Remove a tag
mutation removeTags($code: String!) {
  tagRemove(code: $code) {
    ...tagFields
  }
}

fragment tagFields on Tags {
    code
    address
    type
    name
    description
    status
    lastCommunication
}

*/
