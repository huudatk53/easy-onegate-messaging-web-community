"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let Sockets = require("../../../core/sockets");
let C = require("../../../core/constants");

let _ = require("lodash");

let User = require("./models/user");

module.exports = {
	settings: {
		name: "people",
		version: 1,
		namespace: "people",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: User,

		modelPropFilter: null
		//"code username fullName avatar lastLogin roles, status, stats"
	},

	actions: {
		findByPage: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let filter = ctx.params
						? ctx.params.filter
							? ctx.params.filter
							: {}
						: {};
					let fields = ctx.params
						? ctx.params.fields
							? ctx.params.fields
							: {}
						: {};
					let currentUser = ctx.user;
					let pages = currentUser.profile.fbPages;
					filter = _.assign(filter, {
						"profile.accessPages": {
							$in: currentUser.profile.fbPages
						}
					});
					let query = this.collection.find(filter, fields);

					ctx.queryPageSort(query)
						.exec()
						.then(docs => {
							resolve(this.toJSON(docs));
						})
						.catch(err => {
							reject(err);
						});
				});
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let filter = {};
					let currentUser = ctx.user;
					let pageIds = ctx.params.pageIds;
					if (
						currentUser &&
						currentUser.provider === "facebook" &&
						currentUser.passwordLess
					) {
						filter = _.assign(filter, {
							"fb_pageIds": { $in: pageIds }
						});
						if(ctx.params.condition){
							filter = _.assign(filter, ctx.params.condition);
						}
						let query = this.collection.find(filter);
						ctx.queryPageSort(query)
							.exec()
							.then(docs => {
								resolve(this.toJSON(docs));
							})
							.catch(err => {
								ctx.assertModelIsExist(
									ctx.t("app:UserNotFound")
								);
								resolve(ctx.model);
							});

					} else {
						ctx.assertModelIsExist(ctx.t("app:ParamNotFound"));
						resolve(ctx.model);
					}
				});
			}
		},

		// return a model by ID
		get: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:UserNotFound"));
				return Promise.resolve(ctx.model);
			}
		},

		create: {
			cache: true,
			handler(ctx) {
				this.validateParams(ctx, true);
				let object = new User(ctx.params);
				return object
					.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			cache: true,
			handler(ctx, id) {
				ctx.assertModelIsExist(ctx.t("app:UserNotFound"));
				// this.validateParams(ctx);
				let modelID = id ? id : ctx.modelID;
				let params = ctx.params;
				return this.collection
					.findById(modelID)
					.exec()
					.then(doc => {
						delete params.modelID;
						_.forOwn(params, function(value, key) {
							if (value != null) {
								doc[key] = value;
							}
						});

						return doc.save();
					})
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					});
			}
		},

		updateBy: {
			cache: true,
			handler(ctx, keys, isAnd) {
				// ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
				this.validateParams(ctx);
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function(key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					let conditions = { $or: [] };
					_.forEach(keys, function(key) {
						let obj = [];
						obj[key] = ctx.params[key];
						conditions.$or.push(obj);
					});
				}
				return this.collection.find(searchObj, function(
					err,
					existingUsers
				) {
					if (existingUsers.length == 0) {
						let data = { success: false, msg: "User not found" };
						return this.notifyModelChanges(ctx, "Error", data);
					} else if (existingUsers.length == 1) {
						return this.update(ctx);
					} else {
						let data = {
							success: false,
							msg: "More than 1 to update",
							data: existingUsers
						};
						return this.notifyModelChanges(ctx, "Error", data);
					}
				});
			}
		},
		remove: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:UserNotFound"));

				return this.collection
					.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		}
	},
	// return all model
	/*find: {
			cache: true,
			handler(ctx) {
				return ctx.queryPageSort(User.find({})).exec().then( (docs) => {
					return this.toJSON(docs);
				})
				.then((json) => {
					return this.populateModels(json);
				});
			}
		},*/

	// return a model by ID
	// 	get: {
	// 		cache: true,
	// 		handler(ctx) {
	// 			ctx.assertModelIsExist(ctx.t("app:UserNotFound"));
	// 			return Promise.resolve(ctx.model);
	// 		}
	// 	}
	// }

	methods: {
		assingPageToUser(pageId, userId) {
			return new Promise((resolve, reject) => {
				this.collection
					.findOneAndUpdate(
						{ _id: userId },
						{ $addToSet: { "profile.accessPages": pageId } },
						{ upsert: true, new: true }
					)
					.exec()
					.then(doc => {
						resolve(this.toJSON());
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		addPage(pageId, userId) {
			return new Promise((resolve, reject) => {
				this.collection
					.findOneAndUpdate(
						{ _id: userId },
					{
						  $addToSet: {fb_pageIds: pageId}
						 },
						{ upsert: true, new: true }
					)
					.exec()
					.then(doc => {
						resolve(this.toJSON(doc));
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		removePage(pageId, userId) {
			return new Promise((resolve, reject) => {
				this.collection
					.findOneAndUpdate(
						{ _id: userId },
					{
						 $pull: { fb_pageIds: { $in: [ pageId] }}
						 },
						{ upsert: true, new: true }
					)
					.exec()
					.then(doc => {
						resolve(this.toJSON());
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		promotePageToUser(pageId, userId) {
			return new Promise((resolve, reject) => {
				this.collection
					.findOneAndUpdate(
						{ _id: userId },
						{ $addToSet: { "profile.fbPages": pageId } },
						{ upsert: true, new: true }
					)
					.exec()
					.then(doc => {
						resolve(this.toJSON());
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		getPageEditor(pageId) {
			return new Promise((resolve, reject) => {
				let searchObject = {};
				searchObject["fb_pages_tasks." + pageId] = "MODERATE";
				this.collection
					.findOne(searchObject)
					.exec()
					.then(doc => {
						resolve(this.toJSON(doc));
					})
					.catch(err => {
						reject(err);
					});
			});
		}
	},
	init(ctx) {
		//this.conversationService = ctx.services("conversations");
		// Fired when start the service
		//this.personService = ctx.services("people");
		this.pageService = ctx.services("pages");
		// Add custom error types
		C.append(["ALREADY_VOTED", "NOT_VOTED_YET"], "ERR");
	},

	graphql: {
		query: `
			# users(limit: Int, offset: Int, sort: String): [Person]
			person(code: String): Person
		`,

		types: `
			type Person {
				code: String!
				fullName: String
				username: String
				roles: [String]
				avatar: String
				lastLogin: Timestamp

				posts(limit: Int, offset: Int, sort: String): [Post]
			}
		`,

		mutation: `
		`,

		resolvers: {
			Query: {
				//users: "find",
				person: "get"
			},

			Person: {
				posts(person, args, context) {
					let ctx = context.ctx;
					let postService = ctx.services("posts");
					if (postService)
						return postService.actions.find(
							ctx.copy(
								Object.assign(args, { author: person.code })
							)
						);
				}
			}
		}
	}
};

/*
## GraphiQL test ##

# Get a person
query getPerson {
  person(code: "O5rNl5Bwnd") {
    ...personFields
  }
}


fragment personFields on Person {
  code
  fullName
  username
  roles
  avatar
  lastLogin

  posts(sort: "-createdAt") {
    code
    title
  }
}

*/
