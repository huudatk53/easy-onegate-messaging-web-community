"use strict";

let logger 		= require("../../../core/logger");
let config 		= require("../../../config");
let C 	 			= require("../../../core/constants");

let _ 				= require("lodash");

let Sockets		= require("../../../core/sockets");
let axios		= require("axios");
let  qs  = require("qs");
let FormData = require("form-data");
let personService;

module.exports = {
	settings: {
		name: "session",
		version: 1,
		namespace: "session",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user"
	},

	actions: {
		crmapi(ctx) {

			let request = ctx.params;
			return true
			// return this.post(request);

		},
		crmcall(ctx) {
			// return new Promise((resolve, reject)=>{
			// 	let reqParams = ctx.params;
			// 	this.post(reqParams.request, true, reqParams.server).then(response=>{
			// 		console.log(response);
			// 		resolve(response);
			// 	}).catch(error=>{
			// 		reject(error);
			// 	});

			// });
			return true
		},
		// return my User model
		me(ctx) {
			return Promise.resolve(ctx.user).then( (doc) => {
				return personService.toJSON(doc);
			});
		},

		// return all online users
		onlineUsers(ctx) {
			return Promise.resolve().then(() => {
				return personService.toJSON(_.map(Sockets.userSockets, (s) => s.request.user));
			});
		}
	},

	init(ctx) {
		personService = this.services("people");
	},
	methods: {
		URL(){
			return config.crm.base;
		},
		post(params, ignoreSession, baseUri) {

			let url = (baseUri ? baseUri : this.URL()) + "/xRequest.ashx" ;
			return new Promise((resolve, reject) => {
				if (params.TotalRequests && params.TotalRequests != 0) {
					let arrDelete = [
						"id",
						"leaf",
						"text",
						"cls",
						"level",
						"expanded",
						"uid",
						"parent_uid",
						"LeftIndex",
						"RightIndex",
						"Data",
						"$$hashKey"
					];
					for (let key in arrDelete) {
						if (params.TotalRequests) {
							for (let i = 1; i <= params.TotalRequests; i++) {
								delete params["R" + i + "_" + key];
							}
						} else delete params[key];
					}

				}
				const options = {
					method: "POST",
					headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
					data: qs.stringify(params),
					url,
				};

				//data: {success: true, msg: "Đăng nhập thành công!", Data: "efd23558-f32a-4922-b2a4-fa228070a283"}
				//Data: "efd23558-f32a-4922-b2a4-fa228070a283"
				//msg: "Đăng nhập thành công!"
				//success: true
				//status: 200
				axios(options).then(response=>{
					// console.log(response.data)
					resolve(response.data);
					// let data = response.data
					// if(data) {
					// 	if( data.success) {
					// 		resolve(data)
					// 	} else if(data.UserDS && data.UserDS.User) {
					// 		data.success = true,
					// 		resolve(data)
					// 	}
					// } else {
					// 	reject(response)
					// }
				}).catch(error=>{
					console.log(error);
					reject(error);
				});
			});
		}
	},
	graphql: {

		query: `
			me: Person
			onlineUsers: [Person]
		`,

		mutation: `
		`,

		resolvers: {
			Query: {
				me: "me",
				onlineUsers: "onlineUsers"
			}
		}
	}

};

/*
## GraphiQL test ##

# Get my account
query me {
  me {
    ...personFields
  }
}


# Get list of online users
query getOnlineUser {
  onlineUsers {
    ...personFields
  }
}


fragment personFields on Person {
  code
  fullName
  email
  username
  roles
  verified
  avatar
  lastLogin
  locale

  posts(sort: "-createdAt") {
    code
    title
  }
}

*/
