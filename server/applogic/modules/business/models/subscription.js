"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("pages");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict:false
};

let SubscriptionSchema = new Schema({
	// _id: {
	// 	type: String,
	// 	trim: true,
	// 	unique: true,
	// 	required: "Please fill in a page ID",	
	// },
	fb_id: {
		type: String,
		trim: true,
		unique: true,
		//required: "Please fill in a page ID",	
		
	},
	name: {
		type: String,
		
	},
	fb_memberIds: [{
	 type: String
	}],
	about : {
	 type: String
	},
	access_token:{
	 type: String
	},
	link : {
		type: String
	}, 
	picture:{
		type: Object
	 },
	updated_time : {
		type: Date
	},
	snippet: {
		type:String
	},
	message_count: {
		type: Number,
		default: 0
	},
	participants: {
		type: Array
	},
	senders: {
		type: Array
	},

	can_reply:{
		type: Boolean
	},
	is_subscribed:{
		type: Boolean
	},
	unread_count: {
		type: Number,
		default: 0
	},
	member_count: {
		type: Number,
		default: 0
	},
	member_limit: {
		type: Number,
		default: 0
	},
	expired_date: {
		type: Date
		//default: 0
	},
	isActive: {
		type: Boolean,
		default : false
	},
	// title: {
	// 	type: String,
	// 	trim: true
	// },
	// content: {
	// 	type: String,
	// 	trim: true
	// },
	author: {
		type: String,
		required: "Please fill in an author ID",
		ref: "User"
	},
	// views: {
	// 	type: Number,
	// 	default: 0
	// },
	// voters: [{
	// 	type: String,
	// 	ref: "User"
	// }],
	// votes: {
	// 	type: Number,
	// 	default: 0
	// },
	// editedAt: {
	// 	type: Date
	// },
	// metadata: {}

}, schemaOptions);

SubscriptionSchema.virtual("code").get(function() {
	return this.encodeID();
});
// PageSchema.path('fb_users')
//     .get(function(value) {
//         return value;
//     })
//     .set(function(value) {
// 		if(this.fb_users)
// 		this.fb_users.push(_.intersectionBy(this.fb_users, fbIds));
//     return this.fb_users;
// });
 

// PageSchema.plugin(autoIncrement.plugin, {
// 	model: "Conversation",
// 	startAt: 1
// });

SubscriptionSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

SubscriptionSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

/*
PostSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Subscription = mongoose.model("Subscription", SubscriptionSchema);

module.exports = Subscription;
