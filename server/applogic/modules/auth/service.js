"use strict";
const moment = require("moment");
const randomstring = require("randomstring");

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");
let mongoose = require("mongoose");
let CONTEXT = require("../../../core/context");
let helper = require("../../../core/auth/helper");
let FB = require("fb");
let FbToken		= require("../../../models/fbtoken");
let User        = require("../../../models/user");

module.exports = {
	settings: {
		name: "auth",
		version: 1,
		namespace: "auth",
		rest: true,
		ws: false,
		els: false,
		graphql: false,
		permission: C.PERM_PUBLIC,
		role: "user",
		collection: User
	},

	actions: {
		loginWithFacebook: {
			cache: false,
			handler: (ctx) => {
				return new Promise((resolve, reject) => {
					let apiKey = randomstring.generate({
						length: 32
					});
					let profile;
					const accessToken = ctx.req.body.accessToken;
					const refreshToken = ctx.req.body.refreshToken;

					FB.api("oauth/access_token", {
						grant_type:"fb_exchange_token",
						client_id: config.authKeys.facebook.clientID,
						client_secret: config.authKeys.facebook.clientSecret,
						fb_exchange_token: accessToken,
					}, function (res) {
						if(!res || res.error) {
							reject(new Error(!res ? "error occurred" : res.error));
						}

						FB.setAccessToken(res.access_token);

						const done = (err, user) => {
							if (err) {
								reject(err);
							} else {
								resolve({
									user,
									apiKey
								});
							}
						};

						FB.api("me", "GET", {fields:"id,name,email,location,gender,accounts.limit(1000){id, access_token, tasks}"}, (response)=> {
							let fb_pageIds = [];
							let fb_pages_tasks = {};

							profile = response;

							if (response.accounts && response.accounts.data) {
								for (let i = 0; i<response.accounts.data.length; i++){
									let p = response.accounts.data[i];
									fb_pageIds.push(p.id);
									fb_pages_tasks[p.id] = p.tasks;
								}

								_.forIn(response.accounts.data, (object) => {
									let pTokenModel = {
										type: "p",
										userId: profile.id,
										key: profile.id + "_"+object.id,
										token: object.access_token,
									};
									FbToken.findOneAndUpdate(
										{key: pTokenModel.key, type: pTokenModel.type}, // find a document with that filter
										pTokenModel, // document to insert when nothing was found
										{upsert: true, new: true, runValidators: true}
									).then(data=>{
									}).catch(err=>{
									});

								});

								let uTokenModel = {
									type: "u",
									userId: profile.id,
									key: profile.id,
									token: res.access_token,
								};
								FbToken.findOneAndUpdate(
									{key: uTokenModel.key, type: uTokenModel.type}, // find a document with that filter
									uTokenModel, // document to insert when nothing was found
									{upsert: true, new: true, runValidators: true}
								).then(data=>{
								}).catch(err=>{
								});


							}

							helper.linkToSocialAccountApi({
								apiKey,
								accessToken,
								refreshToken,
								profile,
								done,
								fb_pageIds: fb_pageIds,
								fb_pages_tasks: fb_pages_tasks,
								provider: "facebook",
								email: profile.email,
								userData: {
									name: profile.name,
									gender: profile.gender,
									picture: `https://graph.facebook.com/${profile.id}/picture?type=large`,
									location: (profile.location) ? profile.location.name : null,
								}
							});
						});
					});
				});
			}
		}
	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		search() {
			let filter = {};
			return this.collection.find().exec().then( (docs) => {
				return this.toJSON(docs);
			}).then(json => {
				return this.populateModels(json);
			});
		},
		updateCount(ctx, params) {
			// ctx.assertModelIsExist(ctx.t("app:StatisticNotFound"));
			// this.validateParams(ctx);
			let model = {};
			// if (params.name != null)
			// 	model.name = params.name;

			return this.collection.findOneAndUpdate({fb_id: params.fb_id}, params, { new: false}).exec()
			.then((doc) => {
				return this.toJSON(doc);
			});
			// .then((json) => {
			// 	return this.populateModels(json);
			// })
			// .then((json) => {
			// 	this.notifyModelChanges(ctx, "updated", json);
			// 	return json;
			// });
		}
	},

	init(ctx) {
		// Fired when start the service
	}
};
