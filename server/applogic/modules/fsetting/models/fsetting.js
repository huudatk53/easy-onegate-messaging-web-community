"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("fsetting");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let ContactSchema = new Schema({
	// _id: {
	// 	type: String,
	// 	trim: true,
	// 	unique: true
	// },
	fb_id: {
		type: String,
		trim: true,
		unique: true
	},
	address: {
		type: String,
		trim: true
	},
	fb_link: {
		type: String,
		trim: true
	},
	fb_pages: {
		type: Array
	},
	type: {
		type: String,
		trim: true
	},
	name: {
		type: String,
		trim: true
	},
	description: {
		type: String,
		trim: true,
		"default": ""
	},
	status: {
		type: Number,
		"default": 1
	},
	lastCommunication: {
		type: Date,
		"default": Date.now
	},
	metadata: {}

}, schemaOptions);

ContactSchema.virtual("code").get(function() {
	return this.encodeID();
});

// ContactSchema.plugin(autoIncrement.plugin, {
// 	model: "Contact",
// 	startAt: 1
// });

ContactSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

ContactSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

let fsetting = mongoose.model("fsetting", ContactSchema);

module.exports = fsetting;
