"use strict";

// let ROOT 			= "../../../../";
let config = require("../../../../config");
let logger = require("../../../../core/logger");

let _ = require("lodash");

let db = require("../../../../core/mongo");
let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let hashids = require("../../../../libs/hashids")("notifys");
let autoIncrement = require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let NotifySchema = new Schema(
	{
		// _id: {
		// 	type: String,
		// 	unique: true,
		// 	trim: true
		// },
		text: {
			type: String,
			trim: true
		},
		type: {
			type: String
		},
		fb_page: {
			type: String,
			ref: "Page"
		},
		detail: {
			type: Array,
			trim: true
		},
		content: {
			type: String,
			trim: true
		},
		author: {
			type: String,
			// required: "Please fill in an author ID",
			ref: "User"
		},
		views: {
			type: Number,
			default: 0
		},
		voters: [
			{
				type: String,
				ref: "User"
			}
		],
		votes: {
			type: Number,
			default: 0
		},
		editedAt: {
			type: Date
		},
		metadata: {}
	},
	schemaOptions
);

NotifySchema.virtual("code").get(function() {
	return this.encodeID();
});

// NotifySchema.plugin(autoIncrement.plugin, {
// 	model: "Notify",
// 	startAt: 1
// });

NotifySchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

NotifySchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

/*
NotifySchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Notify = mongoose.model("Notify", NotifySchema);

module.exports = Notify;
