"use strict";

let logger 		= require("../../../core/logger");
let config 		= require("../../../config");
let C 	 		= require("../../../core/constants");

let _			= require("lodash");

let Folder 		= require("./models/folder");

module.exports = {
	settings: {
		name: "folders",
		version: 1,
		namespace: "folders",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Folder,

		modelPropFilter: "code name imageCount fb_page"
	},

	actions: {
		find: {
			cache: false,
			handler(ctx) {
				let filter = {};
				if (ctx.params.condition)
					filter = _.assign(filter, ctx.params.condition);
				let query = Folder.find(filter);
				return ctx.queryPageSort(query).exec().then( (docs) => {
					return this.toJSON(docs);
				}).then(json => {
					return this.populateModels(json);
				});
			}
		},

		// return a model by ID
		get: {
			cache: false,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:FolderNotFound"));
				return Promise.resolve(ctx.model);
			}
		},

		create(ctx) {
			this.validateParams(ctx, true);

			let folder = new Folder({
				name: ctx.params.name,
				imageCount: ctx.params.imageCount,
				fb_page: ctx.params.fb_page,
			});

			return folder.save()
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "created", json);
				return json;
			});
		},

		update(ctx) {
			ctx.assertModelIsExist(ctx.t("app:FolderNotFound"));
			this.validateParams(ctx);
			var model = {}
			if (ctx.params.name != null)
				model.name = ctx.params.name;

			if (ctx.params.imageCount != null) {
				model.$inc = { imageCount: ctx.params.imageCount };
				// doc.imageCount = doc.imageCount + ctx.params.imageCount
			}
			return this.collection.findByIdAndUpdate(ctx.modelID, model, { new: true}).exec()
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "updated", json);
				return json;
			});
		},

		remove(ctx) {
			ctx.assertModelIsExist(ctx.t("app:FolderNotFound"));

			return Folder.remove({ _id: ctx.modelID })
			.then(() => {
				return ctx.model;
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "removed", json);
				return json;
			});
		}

	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("name"))
				ctx.validateParam("name").trim().notEmpty(ctx.t("app:FolderNameCannotBeBlank")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		}
	},

	init(ctx) {
		// Fired when start the service
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
			folders(limit: Int, offset: Int, sort: String): [Folder]
			folder(code: String): Folder
		`,

		types: `
			type Folder {
				code: String!
				address: String
				type: String
				name: String
				description: String
				status: Int
				lastCommunication: Timestamp
			}
		`,

		mutation: `
			folderCreate(name: String!, address: String, type: String, description: String, status: Int): Folder
			folderUpdate(code: String!, name: String, address: String, type: String, description: String, status: Int): Folder
			folderRemove(code: String!): Folder
		`,

		resolvers: {
			Query: {
				folders: "find",
				folder: "get"
			},

			Mutation: {
				folderCreate: "create",
				folderUpdate: "update",
				folderRemove: "remove"
			}
		}
	}

};

/*
## GraphiQL test ##

# Find all folders
query getFolders {
  folders(sort: "lastCommunication", limit: 5) {
    ...folderFields
  }
}

# Create a new folder
mutation createFolder {
  folderCreate(name: "New folder", address: "192.168.0.1", type: "raspberry", description: "My folder", status: 1) {
    ...folderFields
  }
}

# Get a folder
query getFolder($code: String!) {
  folder(code: $code) {
    ...folderFields
  }
}

# Update an existing folder
mutation updateFolder($code: String!) {
  folderUpdate(code: $code, address: "127.0.0.1") {
    ...folderFields
  }
}

# Remove a folder
mutation removeFolder($code: String!) {
  folderRemove(code: $code) {
    ...folderFields
  }
}

fragment folderFields on Folder {
    code
    address
    type
    name
    description
    status
    lastCommunication
}

*/
