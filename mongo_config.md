use admin

db.createUser(
  {
    user: "admin",
    pwd: "12345678",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

db.createUser(
  {
    user: "admin",
    pwd: "12345678",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
db.createUser(
  {
    user: "dbadmin",
    pwd: "123456789",
    roles: [ { role: "dbOwner", db: "mantis" }]
  }
)

db.createUser(
  {
    user: "dbadmin",
    pwd: "123456789",
    roles: [{ role: "userAdminAnyDatabase", db: "admin" }  ]
  }
)

sudo vi /etc/mongod.conf

security:
    authorization: "disabled"

sudo service mongod restart


mongo mongodb://localhost:27017


sudo service mongod stop --config /usr/local/etc/mongod.conf --auth
sudo service mongod start --config /usr/local/etc/mongod.conf

https://cloud.mongodb.com/freemonitoring/cluster/GNGIVX4I5BQPZHSTIKCODZE4XDJKD3BG


mongo --port 27017 -u "admin" -p "12345678" --authenticationDatabase "admin"

mongo --port 27017 -u "dbadmin" -p "12345678" --authenticationDatabase "mantis"


db.updateUser( "dbadmin",
               {

                 roles : [
                           { role: "userAdminAnyDatabase", db: "admin"}
                         ]
                }
             )

 
db.updateUser( "dbadmin",
               {

                 roles : [
                           { role: "dbOwner", db: "mantis5" }
                         ],
                 pwd: "123456789"        
                }
             )            