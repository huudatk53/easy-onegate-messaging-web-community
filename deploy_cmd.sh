#!/bin/bash
version="1.0.7"
echo "--- Remove old images ---"
docker rmi $(docker images -a -q)
echo "---- Build docker image: bosedu/fm:${version} ----"
docker build --rm -t boseduvn/fm:${version} .
echo "---- Push image to docker hub ----"
docker push boseduvn/fm:${version}
echo "---- Deploy to k8s ----"
helm upgrade fm ./helm --atomic